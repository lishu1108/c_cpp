
/**
 * 求亲密数
 */

#include<stdio.h>

int main(int argc, char *argv[]){
    int aSum;   // i 数所有正因子之和
    int bSum;   // aSum 所有正因子之和
    for (int i = 1; i < 500; ++i) { // 穷举 500 以内所有整数
        aSum = 0;
        bSum = 0;
        for (int j = 1; j < i; ++j) {
            if(i % j == 0){ // 若 j 是 i 的因子
                aSum += j;  // 求 i 所有因子之和
            }
        }

        for (int k = 1; k < aSum; ++k) {
            if(aSum % k == 0){  // 若 k 是 aSum 的因子
                bSum += k; // 求 aSum所有因子之和
            }
        }

        // 若 i 与 aSum 是亲密数, 需要满足如下条件
        // 1. i 数 == aSum 因子之和 = bSum
        // 2. aSum 数== i 数因子之和 = aSum, 显然满足
        // 3. i != aSum, 两数不相等
        if(i == bSum && i != aSum && i < aSum){
            printf("%d<==>%d\n",  i, aSum);
        }
    }
}

