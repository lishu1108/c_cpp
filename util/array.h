/*
 * @Author: Thatcher Li
 * @Date: 2020-12-25 10:20:27
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-01-06 18:39:44
 * @Descripttion: An util used for array common operation
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**
 * @description: 输入数组的前 n 个元素
 * @param {int} arr
 * @param {int} n
 * @return {*}
 */
void arr_input(int arr[], int n){
    printf("请输入%d个整数，按空格分隔：\n",n);
    for(int i=0; i<n; i++){
        // 注意 scanf 遇到输入数据与所要求的数据类型不匹配的数据或遇到输入分隔符、指定宽度结束时都会结束当前输入
        scanf("%d",arr+i);
    }
}

/**
 * @description: 输出数组的前 n 个元素
 * @param {int} arr
 * @param {int} n
 * @return {*}
 */
void arr_print(int arr[], int n){
    printf("数组的内容是：\n");
    for(int i=0; i<n; i++){
        printf("%d\t",arr[i]);
    }
    printf("\n");
}

/**
 * @description: 将数组的前 n 个元素用随机数初始化，范围 (1~1000)
 * @param {int} arr
 * @param {int} n
 * @return {*}
 */
void arr_init(int arr[], int n){
    srand(time(0));
    for (int i = 0; i < n; i++)
    {
        arr[i] = rand() % 1000 + 1;
    }
}

/**
 * @description:  将数组两个不同位置上的元素互换
 * @param {int} arr
 * @param {int} i
 * @param {int} j
 * @return {*}
 */
void arr_swap(int arr[], int i, int j){
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}


/**
 * @description: 将数组前 n 个元素所有的值加 1
 * @param {int} arr
 * @param {int} n
 * @return {*}
 */
void arr_inc(int arr[],int n){
    for (int i = 0; i < n; i++)
    {
        arr[i] += 1;
    }
}


/*==========================这部分是二维数组=================================*/

/**
 * @description: 在定义二维数组为形参时，形参的行可以省略，但不能省略二维数组的列下标
 * @param {int} arr
 * @param {int} m
 * @return {*}
 */
void arr2_input(int arr[][3], int m){
    printf("请输入%d行%d列的二维数组，按空格分隔：\n",m,3);
    for(int i=0; i<m; i++){
        for (int j = 0; j < 3; j++)
        {
            scanf("%d",arr[i]+j);
        }
    }
}


/**
 * @description: 
 * @param {int} arr
 * @param {int} m
 * @return {*}
 */
void arr2_print(int arr[][3], int m){// arr[][N] 等价于 int (*arr)[N], 即本质是行指针变量
    printf("二维数组的内容是：\n");
    for(int i=0; i<m; i++){
        for (int j = 0; j < 3; j++)
        {
            printf("%d\t",arr[i][j]);
        }
        printf("\n");
    }
}


/*==========================这部分是字符数组=================================*/

/**
 * @description: 输入字符数组的前 n 个元素
 * @param {char} c
 * @param {int} n
 * @return {*}
 */
void c_arr_input(char c[],int n){
    printf("请输入字符串，字符个数为%d个：\n",n);
    for(int i=0; i<n; i++){
        scanf("%c",c+i);
    }
}


/**
 * @description: 输出字符数组的前 n 个元素
 * @param {char} arr
 * @param {int} n
 * @return {*}
 */
void c_arr_print(char c[], int n){
    printf("字符数组的内容是：\n");
    for(int i=0; i<n; i++){
        printf("%d\t",c[i]);
    }
    printf("\n");
}

/**
 * @description: 
 * @param {char} str
 * @param {int} n
 * @return {*}
 */
void str_input(char str[], int n){
    printf("请输入字符串，字符个数少于%d个：\n",n);
    scanf("%s",str);
}

/**
 * @description: 
 * @param {char} str
 * @return {*}
 */
void str_print(char str[]){
    printf("字符数组的内容是：\n");
    printf("%s",str);
    printf("\n");
}

/*==========================这部分是指针相关=================================*/

/**
 * @description: 
 * @param {int} *arr
 * @param {int} m
 * @param {int} n
 * @return {*}
 */
void arr2_point_print(int *arr, int m ,int n){
    printf("二维数组的内容是：\n");
    for(int i=0; i<m; i++){
        for (int j = 0; j < n; j++)
        {
            printf("%d\t",*arr++);  // 或 *(arr+i*n+j) 或 arr[i*n+j]
        }
        printf("\n");
    }
}


