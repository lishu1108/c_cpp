#include "cn_blogss_core_jni_HelloJni.h"
#include <stdio.h>

/*
 * Class:     cn_blogss_core_jni_HelloJni
 * Method:    get
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_cn_blogss_core_jni_HelloJni_get
  (JNIEnv *env, jobject thiz){
      printf("invoke get in C++\n");
      return env->NewStringUTF("Hello from JNI !");
  }

/*
 * Class:     cn_blogss_core_jni_HelloJni
 * Method:    set
 * Signature: (Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL Java_cn_blogss_core_jni_HelloJni_set
  (JNIEnv *env, jobject thiz, jstring str){
      printf("invoke set in C++\n");
      char* strs = (char*)env->GetStringUTFChars(str,JNI_FALSE);
      printf("%s\n",strs);
      env->ReleaseStringUTFChars(str,strs);
  }