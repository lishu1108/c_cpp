/*
 * @Author: Thatcher Li
 * @Date: 2020-12-25 14:04:04
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-01-04 09:31:30
 * @Descripttion: 二维数组及其应用
 */

#include "array.h"

int main(int argc, char const *argv[])
{
    /*==========声明及初始化==========*/
    int arr[2][3];

    int arr2[2][3] ={
        {2,3,4}
    };

    int arr3[2][3] ={
        1,0,22
    };

    int arr4[2][3];
    arr2_input(arr4,2);

    int arr5[][4] = {
    };

    arr2_print(arr2,2);
    arr2_print(arr3,2);
    arr2_print(arr4,2);
    return 0;
}
