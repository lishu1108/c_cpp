/*
 * @Author: Thatcher Li
 * @Date: 2020-12-24 09:36:56
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-01-06 19:56:33
 * @Descripttion: C 语言的数据类型
 */

#include <stdio.h>
 // 字符 char，占用一个字节
    char word = 'a';

 //注意 C 语言中整形(非静态变量)未初始化，那默认值就是堆栈里面的随机值

// 整形 短整型 2个字节，整形 4个字节，长整形 4个字节(32 位机器)，长长整形 8个字节
short int a = 10;
int b = 100;
long c = 1000;
long long d = 1000000;

// 浮点型，单精度 4个字节(精度为6位有效数字)，双精度 8个字节(精度为15位有效数字)
float e = 10.1;
double f = 10.22;

// 枚举类型

/*==============以上是基本数据类型=========================*/

/* ==============构造类型(数组、结构体、联合体)=========== */


/* =============指针类型================ */


// 空类型

int main(int argc, char const *argv[])
{
    /* code */
    printf("sizeof(char): %d byte\n",sizeof(char));
    printf("sizeof(short int): %d byte\n",sizeof(short int));
    printf("sizeof(int): %d byte\n",sizeof(int));
    printf("sizeof(long int): %d byte\n",sizeof(long int));
    printf("sizeof(long long int): %d byte\n",sizeof(long long int));
    printf("sizeof(float): %d byte\n",sizeof(float));
    printf("sizeof(double): %d byte\n",sizeof(double));
    return 0;
}