/*
 * @Author: Thatcher Li
 * @Date: 2020-12-25 15:24:17
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-01-04 09:31:19
 * @Descripttion: 字符数组
 */

#include <string.h>
#include "array.h"

int main(int argc, char const *argv[])
{
    /*==================字符数组的初始化========================*/
    char c[5] = {'C','h','i','n','a'};

    char c2[10] = {'C','h','i','n','a'};

    char c3[] = {'C','h','i','n','a'};

    char c4[] = {"Hello"};

    char c5[] = "Hello";

    c_arr_print(c,5);
    c_arr_print(c2,5);
    c_arr_print(c3,5);
    c_arr_print(c4,5);
    c_arr_print(c5,5);


    /*==================字符串处理函数========================*/
    // 1. 求字符串长度函数 strlen
    printf("字符串的长度是%d\n",strlen(c)); // 打印结果是 8 ，为什么?

    // 2. 字符串复制函数
    strcpy(c3,c4);
    str_print(c3);

    //3. 字符串连接函数
    strcat(c4,c5);
    str_print(c4);


    /*==================字符数组的输入/输出========================*/
    // 1. 通过循环语句逐个输入或输出数组元素
    char str[10];
    c_arr_input(str,10);
    c_arr_print(str,10);

    // 2. 用格式控制符 %s，按字符串方式进行输入或输出，注意输入时应控制字符串长度不超过数组长度减1，以预留一个字节存放'\0'
    char str2[20];
    str_input(str2,20);
    str_print(str2);

    // 3. 用字符串输入函数 gets、字符串输出函数 puts 进行字符串输入与输出，两个函数均在 stdio.h 中定义
    char str3[30];
    gets(str3);
    puts(str3);

    return 0;
}
