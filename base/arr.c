/*
 * @Author: Thatcher Li
 * @Date: 2020-12-24 18:43:19
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-01-04 09:31:49
 * @Descripttion: 一维数组及其应用
 */

#include "array.h"

#define N 10
#define M 5

// 向函数传递一维数组
void swap(int a, int b){
    int temp = a;
    a = b;
    b = temp;
}


int main(int argc, char const *argv[])
{
    /* code */
    // 声明与初始化的方式
    int a1[5] = {1,2,3,4,5};

    int a2[] = {1,2,3,4};

    int a3[2*3] = {1,2,3,4};

    int a4[N];
    arr_init(a4,N);

    // 指定初始化不支持?
    //int a5[10] = {1,2,[5]=5};

    int a6[M];
    arr_init(a6,M);

    arr_print(a4,N);
    arr_print(a6,M);

    // 向函数传递一维数组，数组元素作为函数实参，当函数中的形参发生修改时，数组元素的值不会受到影响
    swap(a1[0],a1[1]);
    printf("%d\t%d\n",a1[0],a1[1]);
    
    // 向函数传递一维数组，数组名为函数实参，函数中对形参数组的操作会影响实参数组，即传递数组的起始地址
    arr_inc(a6,M);
    arr_print(a6,M);

    printf("sizeof(a1)=%d\n",sizeof(a1));   // 20 byte
    printf("sizeof(int)=%d\n",sizeof(int)); // 4 byte

    return 0;

}





