/*
 * @Author: Thatcher Li
 * @Date: 2020-12-24 11:13:56
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2020-12-25 15:54:17
 * @Descripttion: 参数类型
 */
#include <stdio.h>

// 实际参数(实参)、形式参数(形参)。当函数被调用时，操作系统为形参分配内存，
// 且将对应实际参数的值赋值给形参变量

void change(int x){
    printf("%d\n",x);
    x = 2;
    printf("%d\n",x);
}

void change2(int *x){
    printf("%d\n",*x);
    *x = 2;
    printf("%d\n",*x);
}

int main(int argc, char const *argv[])
{
    /* code */
    int x = 10;
    change(x);
    printf("%d\n",x);
    printf("===================================================\n");
    change2(&x);
    printf("%d\n",x);
    return 0;
}
