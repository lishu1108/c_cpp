/*
 * @Author: Thatcher Li
 * @Date: 2020-12-24 17:06:40
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2020-12-25 15:57:06
 * @Descripttion: 变量的存储类型 
 */

#include <stdio.h>

// static 类型的变量只会被初始化一次
void fun(){
    static int z = 4;
    z++;
    printf("z=%d\n",z);
}

//  extern 型(外部变量)，在全局变量定义位置之前若要使用该全局变量，需要用 extern 关键字对全局变量进行声明
void fun1(){
    extern int num;
    num = 10;
}

int num;

void fun2(){
    num++;
}

int main(int argc, char const *argv[])
{
    /* code */
    for (int i = 0; i < 3; i++)
    {
        fun();
    }
    printf("=======================================\n");
    fun1();
    fun2();
    printf("num=%d\n",num);
    
    return 0;
}
