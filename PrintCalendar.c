
/**
 * 打印某年某个月的万年历
 */

#include <stdio.h>

// 输入的年份，月份
int year, month;
// 闰年每个月的天数
int run[12] = {31,29,31,30,31,30,31,31,30,31,30,31};
// 平年每个月的天数
int ping[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
// 星期日-星期六
char *week[7] = {"日 ", "一  ", "二 ", " 三 ", " 四 ", "  五 ", "  六 "};

/**
 * 判断是否闰年
 * @param year
 * @return
 */
int isLeap(int year){
    if(year%400 == 0 || (year%4 == 0 && year%100 != 0)){
        return 1;
    }
    return 0;
}


/**
 * 计算从 1990.1.1(星期一)到输入年份和月份的总天数
 * @param year, 输入年份
 * @param month, 输入月份
 * @return
 */
int dayCount(int year, int month){
    int sum = 0;

    for (int i = 1990; i < year; ++i) {
        if(isLeap(i)){ // 是闰年
            sum += 366;
        }else{  // 是平年
            sum += 365;
        }
    }

    for (int i = 0; i < month-1; ++i) {
        if(isLeap(year)){ // 是闰年
            sum += run[i];
        }else{  // 是平年
            sum += ping[i];
        }
    }

    return sum;
}

/**
 * 打印星期几
 */
void printWeek(){
    for(int i=0; i<7; i++){
        printf("%s\t", week[i]);
    }
    printf("\n");
}

 /**
  * 打印某年某月的日历
  * @param count, 总天数
  * @param year, 输入年份
  * @param month, 输入月份
  */
void printCalendar(int count, int year, int month){
    int fw, days;
    // fw = 本月一号是星期几
    fw = count%7 + 1;
    // 首行要打印的天数
    days = 7 - fw;

    // 首行打印 fw 个空格
     for (int i = 0; i < fw; ++i) {
         printf("\t");
     }

     if(isLeap(year)){
         for (int i = 1; i < run[month-1] + 1; ++i) {
             printf("%d\t", i);
             if(days == i || (i- days)%7 == 0){
                 printf("\n");
             }
         }
         printf("\n");
     }else{
         for (int i = 1; i < ping[month-1] + 1; ++i) {
             printf("%d\t", i);
             if(days == i || (i- days)%7 == 0){
                 printf("\n");
             }
         }
         printf("\n");
     }
}

int main(int argc, char *argv[]){
    while (1){
        scanf("%d %d", &year, &month);
        printf("\n");
        printWeek();
        printCalendar(dayCount(year, month), year, month);
    }
    return 0;
}



