/*
 * @Author: Thatcher Li
 * @Date: 2021-01-06 19:42:57
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-01-08 15:14:59
 * @Descripttion: 顺序表增删改查基本测试
 */
#include "nodetype.h"

int main(int argc, char const *argv[]){
    Sqlist l;
    // 初始化
    sqlist_init(&l);

    // 插入元素
    sqlist_insertElem(&l,sqlist_find_Elem(&l,3),3);
    sqlist_insertElem(&l,0,10);
    sqlist_insertElem(&l,1,3);
    sqlist_insertElem(&l,sqlist_find_Elem(&l,5),5);
    sqlist_insertElem(&l,sqlist_find_Elem(&l,1),1);
    sqlist_insertElem(&l,sqlist_find_Elem(&l,8),8);
    sqlist_print(&l);

    // 删除元素
    int r;
    sqlist_delElem(&l,2,&r);    // 删除下标为2的元素
    printf("删除的元素是：%d\n",r);
    sqlist_print(&l);

    sqlist_recycle_moveK(&l,l.length,3);
    sqlist_print(&l);

    return 0;
}
