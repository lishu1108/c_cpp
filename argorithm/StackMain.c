/*
 * @Author: Thatcher Li
 * @Date: 2021-02-05 14:57:33
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-02-05 16:28:58
 * @Descripttion: 栈的基本操作测试
 */

#include "nodetype.h"

int main(int argc, char const *argv[]){
    // 顺序栈
    SqStack sqStack;
    sqStack_init(&sqStack);
    sqStack_push(&sqStack,2);
    sqStack_push(&sqStack,4);
    sqStack_push(&sqStack,6);
    sqStack_push(&sqStack,2);
    sqStack_push(&sqStack,4);
    sqStack_push(&sqStack,6);
    sqStack_print(&sqStack);


    sqStack_pop(&sqStack);
    sqStack_pop(&sqStack);
    sqStack_pop(&sqStack);
    sqStack_print(&sqStack);

    sqStack_pop(&sqStack);
    sqStack_pop(&sqStack);
    sqStack_pop(&sqStack);
    sqStack_print(&sqStack);

    // 链栈
    LStack *lStack;
    lStack = lStack_init(lStack);
    lStack_push(lStack,1);
    lStack_push(lStack,2);
    lStack_push(lStack,2);
    lStack_push(lStack,3);
    lStack_print(lStack);

    lStack_pop(lStack);
    lStack_pop(lStack);
    lStack_print(lStack);

    return 0;
}
