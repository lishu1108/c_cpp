/*
 * @Author: your name
 * @Date: 2021-07-15 14:13:24
 * @LastEditTime: 2021-07-16 16:01:00
 * @LastEditors: Please set LastEditors
 * @Description: 字符串节点类型、常用方法
 */

#define MAXSIZE 20
#include <stdio.h>
#include <stdlib.h>

/**
 * @description: 定长顺序存储串
 * @param {*}
 * @return {*}
 */
typedef struct{
    char str[MAXSIZE + 1];
    int length;
}StrFix;


/**
 * @description: 可变长存储串
 * @param {*}
 * @return {*}
 */
typedef struct{
    char *ch;
    int length;
}StrMutabled;


/**
 * @description: 字符串赋值
 * @param {StrMutabled} *str
 * @param {char} *ch
 * @return {*}
 */
int strAssign(StrMutabled *strMutabled, char *ch){
    int len = 0;
    char *c = ch;
    while(*c){  // 求 ch 长度
        ++len;
        ++c;
    }

    if(len == 0){   // ch 是空串
        strMutabled->ch = NULL;
        strMutabled->length = 0;
        return 1;
    }else{
        strMutabled->ch = ch;
        strMutabled->length = len;
        return 1;
    }
    return 0;
}


