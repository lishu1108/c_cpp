/*
 * @Author: your name
 * @Date: 2021-07-15 14:12:38
 * @LastEditTime: 2021-07-16 15:50:44
 * @LastEditors: Please set LastEditors
 * @Description: 字符串
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Str.h"

int main(int argc, char const *argv[]){
    /* code */
    char str[] = "hello nick.";
    char str2[] = "Hi michael, nice to see you !";
    printf("str 的长度: %d\n", strlen(str));
    
    char *str3 = "Ba.";
    char *str4 = "Do you like banana?";
    printf("str3 的长度: %d\n", strlen(str3));
    printf("str4 的长度: %d\n", strlen(str4));
    str4 = str3;
    printf("str4 的长度: %d\n", strlen(str4));

    int len = 0;
    char *p = str4;
    while(*p){
        printf("%c\t",*p);
        len++;
        p++;
    }
    printf("\n");
    printf("str4 的长度: %d\n", len);


    printf("==================开始测试串结构体===================\n");
    char *c1 = "test1 changed.";
    StrMutabled s1;
    s1.ch = "I'm test1";
    printf("s1 的内容: %s，长度：%d\n", s1.ch,strlen(s1.ch));
    s1.ch = c1;
    printf("s1 的内容: %s，长度：%d\n", s1.ch,strlen(s1.ch));
    strAssign(&s1,"method change.");
    printf("s1 的内容: %s，长度：%d\n", s1.ch,strlen(s1.ch));
    return 0;
}

