/*
 * @Author: Thatcher Li
 * @Date: 2021-01-07 11:00:16
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-07-16 15:41:14
 * @Descripttion: 单链表的基本操作测试
 */
#include "nodetype.h"

int main(int argc, char const *argv[]){
    struct LNode *L1,*L2,*merge;
    int a1[] = {1,3,8,11,32,83};
    L1 = link_init_tail(L1,a1,sizeof(a1)/sizeof(int));
    link_print(L1);

    int a2[] = {2,4,22,35,77,100,103};
    L2 = link_init_tail(L2,a2,sizeof(a2)/sizeof(int));
    link_print(L2);

    // 合并单链表，升序
    //merge = link_merge_asc(L1,L2);
    //link_print(merge);

    // 合并单链表，降序
    //merge = link_merge_des(L1,L2);
    //link_print(merge);

    // 删除 i 位置上的链表元素
    link_delElem(L2,7);
    link_delElem(L2,7);
    link_print(L2);

    return 0;


}

