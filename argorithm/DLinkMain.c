/*
 * @Author: Thatcher Li
 * @Date: 2021-01-08 10:36:20
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-03-11 09:30:54
 * @Descripttion: 
 */

#include "nodetype.h"

int main(int argc, char const *argv[]){
    DLNode *dL1, *dL2;
    int a1[] = {2,12,18,29,33};
    int a2[] = {1,11,28,39,43};

    dL1 = dlink_init_tail(dL1,a1,sizeof(a1)/sizeof(int));
    dL2 = dlink_init_tail(dL1,a2,sizeof(a2)/sizeof(int));

    dL1 = dlink_print_head(dL1);
    dL1 = dlink_print_tail(dL1);

    //  插入与删除操作
    dlink_print_head(dL2);
    dlink_insertElem(dL2,6,200);    // 第6个位置上插入 200
    dlink_insertElem(dL2,2,100);    // 第2个位置上插入 100
    dlink_print_head(dL2);

    dlink_delElem(dL2,7); //删除最后一个
    dlink_print_head(dL2);

    return 0;
}
