/*
 * @Author: Thatcher Li
 * @Date: 2021-02-06 15:39:23
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2021-07-16 16:16:53
 * @Descripttion: 
 */

#include "nodetype.h"

int main(int argc, char const *argv[]){
    // 顺序队列
    printf("=================开始测试顺序队列=======================\n");
    SqQueue sqQueue;
    sqQueue_init(&sqQueue);
    sqQueue_enqueue(&sqQueue,2);
    sqQueue_enqueue(&sqQueue,3);
    sqQueue_enqueue(&sqQueue,4);
    sqQueue_enqueue(&sqQueue,8);
    sqQueue_enqueue(&sqQueue,12);
    sqQueue_print(&sqQueue);

    sqQueue_dequeue(&sqQueue);
    sqQueue_dequeue(&sqQueue);
    sqQueue_dequeue(&sqQueue);
    sqQueue_print(&sqQueue);

    sqQueue_enqueue(&sqQueue,8);
    sqQueue_enqueue(&sqQueue,12);
    sqQueue_print(&sqQueue);

    // 链式队列
    printf("=================开始测试链式队列=======================\n");
    LQueue *lQueue;
    lQueue = lQueue_init(lQueue);
    lQueue_enqueue(lQueue,2);
    lQueue_enqueue(lQueue,1);
    lQueue_enqueue(lQueue,5);
    lQueue_enqueue(lQueue,8);
    lQueue_print(lQueue);
    lQueue_dequeue(lQueue);
    lQueue_dequeue(lQueue);
    lQueue_dequeue(lQueue);
    lQueue_print(lQueue);
    lQueue_dequeue(lQueue);
    lQueue_print(lQueue);
    lQueue_dequeue(lQueue);
    lQueue_print(lQueue);
    lQueue_enqueue(lQueue,81);
    lQueue_enqueue(lQueue,20);
    lQueue_print(lQueue);

    return 0;
}
