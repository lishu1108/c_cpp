/*
 * @Author: Thatcher Li
 * @Date: 2021-06-29 11:03:25
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2021-06-29 11:36:47
 * @Descripttion: 
 */

#include <stdio.h>
#include <stdlib.h>
#define MAXSIZE 5

/**
 * 顺序表结构体
 */
typedef struct {
    int data[MAXSIZE];
    int length
}Sqlist;

/**
 * 单链表结构体
 */
typedef struct LNode{
    int value;
    struct LNode *next;
}LNode;

/**
 * 双链表结构体
 */
typedef struct DNode {
    int data;
    struct DNode *prior;
    struct DNode *next;
}DNode;

/**
 * 顺序栈 
 */
typedef struct SqStack {
    int top;
    int data[MAXSIZE];
};

/**
 *  链式栈
 */
typedef struct LStack {
    int data;
    struct LStack *next;
}LStack;

void sqlist_init(Sqlist *sqList){
    sqList->length = 0;
}

void sqlist_

int main(int argc, char const *argv[]){
    /* code */

    return 0;
}

