/*
 * @Author: Thatcher Li
 * @Date: 2020-12-30 15:15:42
 * @LastEditors: Thatcher Li
 * @LastEditTime: 2020-12-30 15:44:01
 * @Descripttion: 
 */
# include "../../util/array.h"

/**
 * @description: 简单选择排序
 * @param {int} arr
 * @return {*}
 */
void select_sort(int arr[],int len){
    int minIndex;
    for (int i = 0; i < len-1; i++){
        minIndex = i;
        for (int j = i+1; j < len; j++){
            if(arr[j] < arr[minIndex]){
                minIndex = j;
            }
        }
        if (minIndex != i){
            arr_swap(arr,minIndex,i);
        }
    }
    
}

int main(int argc, char const *argv[])
{
    int arr[] = {8, 2, 9, 4, 3, 1};
    select_sort(arr,sizeof(arr)/sizeof(int));
    arr_print(arr,6);
    return 0;
}
